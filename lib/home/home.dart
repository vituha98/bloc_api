import 'package:bloc_api/base_widget.dart';
import 'package:bloc_api/home/network/connection_aware.dart';
import 'package:bloc_api/home/splash_screen.dart';
import 'package:bloc_api/services/bored_service.dart';
import 'package:bloc_api/services/connectivity_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc.dart';

class HomePage extends BaseStatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends BaseState with ConnectionAware {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc(
          RepositoryProvider.of<BoredService>(context),
          RepositoryProvider.of<ConnectivityService>(context))
        ..add(AppInitializingEvent()),
      child: Scaffold(
          appBar: AppBar(
            title: const Text('Activities for bored people'),
          ),
          body: BlocBuilder<HomeBloc, HomeState>(
            builder: (context, state) {
              if (state is AppInitializingState) {
                print('state is AppInitializingState');
                return const SplashScreen();
              }
              if (state is HomeLoadingState) {
                print('state is HomeLoadingState');
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (state is HomeLoadedState) {
                print('state is HomeLoadedState');
                return Column(
                  children: [
                    Text(state.activityName),
                    Text(state.activityType),
                    Text(state.participants.toString()),
                    ElevatedButton(
                        onPressed: () {
                          if (networkState == NetworkState.connected) {
                            BlocProvider.of<HomeBloc>(context)
                                .add(LoadApiEvent());
                          } else {
                            BlocProvider.of<HomeBloc>(context)
                                .add(NoInternetEvent());
                          }
                        },
                        child: const Text('LOAD NEXT')),
                  ],
                );
              }
              if (state is NoInternetState) {
                return const Center(child: Text('need internet connection'));
              }
              return Container();
            },
          )),
    );
  }

  @override
  void onNetworkConnected() {
    ScaffoldMessenger.of(context).clearSnackBars();
  }

  @override
  void onNetworkDisconnected() {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text("No Network Connection"),
      duration: Duration(hours: 5),
    ));
  }
}
