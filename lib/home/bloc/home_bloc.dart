import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_api/services/bored_service.dart';
import 'package:bloc_api/services/connectivity_service.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final BoredService _boredService;
  final ConnectivityService _connectivityService;

  HomeBloc(this._boredService, this._connectivityService)
      : super(AppInitializingState()) {
    print('HomeBloc - construcor');

    on<AppInitializingEvent>((event, emit) async {
      print('on<AppInitializingEvent>');
      final activity = await _boredService.getBoredActivity();
      await Future.delayed(const Duration(seconds: 6)).then((_) => emit(
          HomeLoadedState(
              activity.activity, activity.type, activity.participants)));
    });

    on<LoadApiEvent>((event, emit) async {
      print('on<LoadApiEvent>');
      emit(HomeLoadingState());
      final activity = await _boredService.getBoredActivity();
      emit(HomeLoadedState(
          activity.activity, activity.type, activity.participants));
    });

    on<NoInternetEvent>((event, emit) async {
      emit(NoInternetState());
    });
  }
}
