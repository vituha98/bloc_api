import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';

import 'home/network/connection_aware.dart';

abstract class BaseStatefulWidget extends StatefulWidget {
  const BaseStatefulWidget({Key? key}) : super(key: key);
}

abstract class BaseState<T extends BaseStatefulWidget> extends State<T> {
  BaseState();

  @override
  @mustCallSuper
  void initState() {
    if (this is ConnectionAware) {
      ConnectionAware awareObj = this as ConnectionAware;
      awareObj.connectivitySubscription =
          Connectivity().onConnectivityChanged.listen((event) {
        if (event == ConnectivityResult.mobile ||
            event == ConnectivityResult.wifi) {
          awareObj.networkState = NetworkState.connected;
          print('networkState = connected');
          awareObj.onNetworkConnected();
        } else if (event == ConnectivityResult.none) {
          awareObj.networkState = NetworkState.disconnected;
          print('networkState = disconnected');
          awareObj.onNetworkDisconnected();
        }
      });
    }
    super.initState();
  }

  @override
  @mustCallSuper
  void dispose() {
    if (this is ConnectionAware) {
      (this as ConnectionAware).connectivitySubscription?.cancel();
    }
    super.dispose();
  }
}
